using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mvc_crud.Data;
using mvc_crud.Models.DB;


namespace mvc_crud.Controllers
{
    public class ContatoController : Controller
    {
        private readonly ILogger<ContatoController> _logger;
        private readonly appContext _context;
        
        public ContatoController(ILogger<ContatoController> logger, appContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {   
            IList<Contato> contatos = _context.Contatos.ToList(); 
            return View(contatos);
        }

        public IActionResult Novo()
        {
            return View();
        }

    [HttpPost]
        public IActionResult Novo([Bind("Id,Nome,Email,Telefone")]Contato novoContato)
        {
            _context.Contatos.Add(novoContato);
            _context.SaveChanges();
            
            return RedirectToAction(nameof(Index));
        }
    }
}